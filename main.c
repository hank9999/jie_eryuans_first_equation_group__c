#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main() {

	double a = 0.0, b = 0.0, c = 0.0, d = 0.0, e = 0.0, f = 0.0, x = 0.0, y = 0.0;
	double e2 = 0.0, f2 = 0.0;
	double x2 = 0.0, y2 = 0.0;

	printf("二元一次方程计算程序\n");
	printf("一般方程组形式\n");
	printf("{ax+by=c\n{dx+ey=f\n");
	printf("版本2.7 更新判断程序 \n暂不支持无限小数，过长小数，无理数，错误方程！\n");
	printf("例子：1,1,2,1,-1,0\n现在请输入a,b,c,d,e,f:\n");

	scanf_s("%lf,%lf,%lf,%lf,%lf,%lf", &a, &b, &c, &d, &e, &f);
	if ((a / d) != (b / e))
	{
		printf("\n该程组有一组解\n");
		//算法一
		if (a != 0)
		{
			e2 = e - (d / a)*b;
			f2 = f - (d / a)*c;
		}
		else {
			y = c / b;
			x = (f - e * y) / d;
			printf("算法一\n原方程组的解\nx=%lf\ny=%lf\n分数形式\nx=%lf/%lf\ny=%lf/%lf\n", x, y, (f - e * y), d, c, b);
		}
		if (a != 0 && e2 != 0)
		{
			y = f2 / e2;
			x = (c - b * y) / a;
			printf("算法一\n原方程组的解\nx=%lf\ny=%lf\n分数形式\nx=%lf/%lf\ny=%lf/%lf\n", x, y, (c - b * y), a, f2, e2);
		}
		else
		{
			printf("算法一暂时不支持");
		}

		//算法二

		x2 = (c*e - b * f) / (a*e - b * d); //x
		y2 = (c*d - a * f) / (b*d - a * e); //y

		printf("算法二\n原方程组的解\nx=%lf\ny=%lf\n分数形式\nx=%lf/%lf\ny=%lf/%lf\n", x2, y2, (c*e - b * f), (a*e - b * d), (c*d - a * f), (b*d - a * e));

	}
	else if ((a / d) == (b / e) == (c / f))
	{
		printf("\n该方程组有无数组解\n");
	}
	else if ((a / d) == (b / e) != (c / f))
	{
		printf("\n该方程组无解\n");
	}



	system("pause");
	return 0;
}